CREATE DATABASE  colegio;
USE  colegio;

CREATE TABLE alumnos(
id int  primary key auto_increment,
nombre varchar(15) ,
apellido varchar (30),
edad integer,
fecha date
);