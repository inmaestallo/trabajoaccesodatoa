DELIMITER //
CREATE PROCEDURE crearTablaColegio()
BEGIN
    CREATE TABLE profesores
    (
        Id       int primary kEY auto_increment,
        nombre   varchar(30) unique,
        apellido varchar(40),
        edad     integer,
        fecha    date
    );
END //
