package colegio;

public class Principal {
    /**
     * Creas el main  y creas una nueva vista , un nuevo modelo y un controldador al que le pasas
     * vista y modelo
     * @param args
     */
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista,modelo);
    }
}
