package colegio;

public class Colegio {

  String nombre;
    String apellido;
    int edad;

    /**
     * Construtor aquí le paso los tres parametros
     * @param nombew
     * @param apellido
     * @param edad
     */
    public Colegio(String nombew, String apellido, int edad) {
        this.nombre = nombew;
        this.apellido = apellido;
        this.edad = edad;
    }

    /**
     * Gettere nombre
     * @return nombre;
     */
    public String getNombew() {
        return nombre;
    }

    /**
     * Setter nombre
     * @param nombew
     */

    public void setNombew(String nombew) {
        this.nombre = nombew;
    }
    /**
     * Gettere apellido
     * @return apellido;
     */
    public String getApellido() {
        return apellido;
    }
    /**
     * Setter apellido
     * @param apellido
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    /**
     * Gettere edad
     * @return edad;
     */
    public int getEdad() {
        return edad;
    }
    /**
     * Setter nombre
     * @param edad
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }

 
    
}
