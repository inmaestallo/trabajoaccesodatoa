package colegio;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

import static com.sun.java.accessibility.util.AWTEventMonitor.addActionListener;

public class Controlador implements ActionListener,TableModelListener {

    private Vista vista;
    private Modelo modelo;

    private enum tipoEstado {conectado, desconectado};
    private tipoEstado estado;

    /**
     * Constructor le paso la vista y el modelo
     * @param vista
     * @param modelo
     */

    public Controlador(Vista vista, Modelo modelo) {
        this.modelo = modelo;
        this.vista = vista;
        estado = tipoEstado.desconectado;

        iniciarTabla();
        addActionListener(this);
        addTableModelListeners(this);
    }

    /**
     * AddActionLostener
     * @param listener
     */
    private void addActionListener(ActionListener listener){
        vista.btnBuscar.addActionListener(listener);
        vista.btnEliminar.addActionListener(listener);
        vista.btnNuevo.addActionListener(listener);
        vista.itemConectar.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.btncrearTabla.addActionListener(listener);
        vista.itemCrearTabla.addActionListener(listener);
    }

    /**
     * addTableModelListeners
     * @param listener
     */
    private void addTableModelListeners(TableModelListener listener) {
        vista.dtm.addTableModelListener(listener);
    }

    /**
     * Método que actualiza las columnas de la tabla
     * @param e
     */
    @Override
    public void tableChanged(TableModelEvent e) {
        if(e.getType() == TableModelEvent.UPDATE){
            System.out.println("actualizada");
            int filaModificada = e.getFirstRow();
            try {
                modelo.modificarAlumno((Integer)vista.dtm.getValueAt(filaModificada,0),
                        (String)vista.dtm.getValueAt(filaModificada,1), (String)vista.dtm.getValueAt(filaModificada,2),
                        ((Integer) vista.dtm.getValueAt(filaModificada,3)),((LocalDate) vista.dtm.getValueAt(filaModificada,4)));
                vista.lblAccion.setText("Columna Actualizada");
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }


    }


    /**
     * Método en el  insertar una tabla , buscas, eliminas, creas una tabla nueva,sales ,conectas y desconectas de
     * una base de datos
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch(comando){
            case "Nuevo":
                try {
                    modelo.insertarAlumno(vista.txtNombre.getText(),vista.txtApellido.getText(),
                            Integer.parseInt(vista.txtEdad.getText()), vista.fechaDatePicker.getDate());
                    cargarFilas(modelo.obtenerDatos());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;

            case "Buscar":

                try {
                    cargarFilas(modelo.buscarAlumno(""));
                    cargarFilas(modelo.buscarAlumno(vista.txtBuscar.getText()));
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                  vista.lblAccion.setText("fila encontrada");

                break;
            case "Eliminar":

                try {
                    int filaBorrar = vista.table1.getSelectedRow();
                    int idBorrar = (Integer)vista.dtm.getValueAt(filaBorrar,0);
                    modelo.eliminarAlumno(idBorrar);
                    vista.dtm.removeRow(filaBorrar);
                    vista.lblAccion.setText("Fila Eliminada");
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

                break;

            case "Crear tabla" :

                try{
                    modelo.crearTabla(vista.txtCrearTabla.getText());
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

                break;
            case "CreartablaColegio":
                try {
                    modelo.crearTablaColegio();
                    vista.lblAccion.setText("tabla de colegio creada");
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;

            case "Salir":
                System.exit(0);
                break;

            case "Conectar":
                if(estado == tipoEstado.desconectado) {
                    try {
                        modelo.conectar();
                        vista.itemConectar.setText("Desconectar");
                        estado = tipoEstado.conectado;
                        cargarFilas(modelo.obtenerDatos());

                    } catch (SQLException e1) {
                        JOptionPane.showMessageDialog(null,"Error de conexion","Error",JOptionPane.ERROR_MESSAGE);
                        e1.printStackTrace();

                    }

                }else{
                    try {
                        modelo.desconectar();
                        vista.itemConectar.setText("Conectar");
                        estado = tipoEstado.desconectado;
                        vista.lblAccion.setText("Desconectado");
                    } catch (SQLException e1) {
                        JOptionPane.showMessageDialog(null,"Error de desconexion","Error",JOptionPane.ERROR_MESSAGE);
                        e1.printStackTrace();

                    }

                }

                break;
        }

    }

    /**
     * Iniciar una tabla
     */

    private void iniciarTabla(){
        String[] headers = {"id", "nombre", "apellido", "edad","fecha"};
        vista.dtm.setColumnIdentifiers(headers);
    }

    /**
     * Método en el que cargas los  datos  de la fila
     * @param resultSet
     * @throws SQLException
     */
    private void cargarFilas(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[5];
        vista.dtm.setRowCount(0);

        while(resultSet.next()){
            fila[0] = resultSet.getObject(1);
            fila[1] = resultSet.getObject(2);
            fila[2] = resultSet.getObject(3);
            fila[3] = resultSet.getObject(4);
            fila[4] = resultSet.getObject(5);



            vista.dtm.addRow(fila);

        }

        if(resultSet.last()){
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(resultSet.getRow()+" filas cargadas");
        }




    }

}
