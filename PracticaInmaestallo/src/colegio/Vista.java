package colegio;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class Vista {
    private JPanel panel1;
    JTextField txtNombre;
    JTextField txtApellido;
    JTextField txtEdad;
    JButton btnBuscar;
    JButton btnNuevo;
    JButton btnEliminar;
     JTextField txtBuscar;
    JTable table1;
     JLabel lblAccion;
    DatePicker fechaDatePicker;
     JButton btncrearTabla;
    JTextField txtCrearTabla;
    JFrame frame;
    DefaultTableModel dtm;
    JMenuItem itemConectar;
    JMenuItem itemSalir;
    JMenuItem itemCrearTabla;

    /**
     * Constructor al que le pones el frame
     */

    public Vista() {
        frame = new JFrame("Colegio BBDD");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        dtm = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                if(column == 0){
                    return false;
                }
                return true;
            }
        };




     table1.setModel(dtm);

        crearMenu();
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Método en el que creas el menu
     */
    private void crearMenu(){
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("Conectar");
        itemCrearTabla=new JMenuItem("Crear tabla colegio");
        itemCrearTabla.setActionCommand("CreartablaColegio");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");

        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemConectar);
        menuArchivo.add(itemSalir);
        menuArchivo.add(itemCrearTabla);

        JMenuBar barraMenu = new JMenuBar();
        barraMenu.add(menuArchivo);

        frame.setJMenuBar(barraMenu);
    }

}
