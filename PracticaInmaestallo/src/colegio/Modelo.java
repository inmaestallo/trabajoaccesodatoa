package colegio;

import java.sql.*;
import java.time.LocalDate;



public class Modelo {

    private Connection conexion;
    private Vista vista;

    /**
     *  Método en el con el que conectas a la base de datos
     * @throws SQLException
     */
    public void conectar() throws SQLException {
        conexion = null;

        conexion = DriverManager.getConnection("jdbc:mysql://localhost:3307/colegio", "root", "");
    }

    /**
     * Crear tabla
     * @throws SQLException
     */
    public void crearTablaColegio() throws SQLException {
        conexion = null;

        conexion = DriverManager.getConnection("jdbc:mysql://localhost:3307/colegio","root","");
        String sentenciaSql="call crearTablaColegio()";
        CallableStatement procedimiento=null;
        procedimiento=conexion.prepareCall(sentenciaSql);
        procedimiento.execute();
    }

    /**
     *  Método en el con el que desconectas la base de datos
     * @throws SQLException
     */
    public void desconectar() throws SQLException {
        conexion.close();
        conexion = null;
    }

    /**
     * Método con el que obtienes los datos de la tabla
     * @return resultado
     * @throws SQLException
     */
    public ResultSet obtenerDatos() throws SQLException {
        if (conexion == null)
            return null;

        if (conexion.isClosed())
            return null;

        String consulta = "SELECT * FROM alumnos";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    /**
     * Método con el que insertas los  datos de los alumnos a una tabla
     * @param nombre
     * @param apellido
     * @param edad
     * @param fecha
     * @return numeroRegistros
     * @throws SQLException
     */
    public int insertarAlumno(String nombre, String apellido, int edad,LocalDate fecha) throws SQLException {
        if (conexion == null)
            return -1;

        if (conexion.isClosed())
            return -2;

        String consulta = "INSERT INTO alumnos(nombre,apellido,edad,fecha)" +
                "VALUES (?, ?, ?,?)";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, nombre);
        sentencia.setString(2, apellido);
        sentencia.setInt(3, edad);
        sentencia.setDate(4, Date.valueOf(fecha));



        int numeroRegistros = sentencia.executeUpdate();

        if (sentencia != null) {
            sentencia.close();
        }

        return numeroRegistros;
    }

    /**
     * Método con el que eliminas un alumno de la tabla
     * @param id
     * @return resultado
     * @throws SQLException
     */
    public int eliminarAlumno(int id) throws SQLException {


        if (conexion == null)
            return -1;

        if (conexion.isClosed())
            return -2;



        String consulta = "DELETE FROM alumnos WHERE id = ?";

        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        sentencia.setInt(1, id);

        int resultado = sentencia.executeUpdate();

        if (sentencia != null) {
            sentencia.close();
        }

        return resultado;

    }

    /**
     * Modifcas un alumno de la tabla
     * @param id
     * @param nombre
     * @param apellido
     * @param edad
     * @param fecha
     * @return resultado
     * @throws SQLException
     */

    public int modificarAlumno(int id, String nombre, String apellido, int edad, LocalDate fecha) throws SQLException {
        if (conexion == null)
            return -1;

        if (conexion.isClosed())
            return -2;

        String consulta = "UPDATE alumnos SET nombre = ?, apellido = ?, " +
                "edad = ? ,fecha=? WHERE id = ?";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, nombre);
        sentencia.setString(2, apellido);
        sentencia.setInt(3, edad);
        sentencia.setDate(4, Date.valueOf(fecha));
        sentencia.setInt(5, id);


        int resultado = sentencia.executeUpdate();

        if (sentencia == null)
            sentencia.close();

        return resultado;
    }

    /**
     * Método con el que buscas un alumno
     * @param valor
     * @return resultado
     * @throws SQLException
     */
   public ResultSet buscarAlumno(String valor) throws SQLException {


        if (conexion == null)
            return null;

        if (conexion.isClosed())
            return null;

    String consulta = "SELECT * FROM alumnos WHERE CONCAT(id ,nombre,apellido,edad,fecha ) LIKE'%" +valor+"%'";

    PreparedStatement sentencia = null;

     sentencia = conexion.prepareStatement(consulta);
    ResultSet resultado = sentencia.executeQuery( consulta);




        return resultado;

    }

    /**
     * Método con el que creas una tabla nueva
     * @param profesores
     * @return resultao
     * @throws SQLException
     */
    public int  crearTabla(String profesores) throws SQLException {
        PreparedStatement sentencia = null;
        String sql="CREATE TABLE IF NOT EXISTS " + profesores + "(id INT,nombre VARCHAR(30),apellido VARCHAR(30),edad INT, posicion VARCHAR (30))";

        sentencia=this.conexion.prepareStatement(sql);



        int reaultao=sentencia.executeUpdate(sql);


        return reaultao;

    }



}
